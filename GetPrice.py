import requests
import json
import datetime
from random import randint
from bs4 import BeautifulSoup
import data


#give file unique date-time stamp so that it doesn't overwrite anything
now=str(datetime.datetime.now())
unique=''
for char in now:
	if char in '0123456789':
		unique += char

file = open(f'AutoFiles\{unique}SuggestedPrices.csv','w')



def getprice(x):
    jbquestion = f'https://www.justbooks.co.uk/search/?author=&title=&lang=en&isbn={x}&new=1&used=1&ebooks=0&destination=gb&currency=GBP&mode=basic&st=sr&ac=qr'
    jbanswer = requests.get(jbquestion, headers={'Accept': 'text/plain'})
    jbtxt = jbanswer.text
    soup = BeautifulSoup(jbtxt, 'html.parser')

    soupauthor = soup.find_all(attrs={'itemprop':"author"})
    souptitle = soup.find_all(attrs={'itemprop':"name"})
    souppublisher = soup.find_all(attrs={'itemprop':"publisher"})
    soupprice = soup.find(attrs={'class':"results-price"})

    soupprices = soup.find_all(attrs={'class':"results-price"})

    total = 0
    nurmber = len(soupprices)

    for soupprice in soupprices:
        y = ''
        for char in soupprice.get_text():
            if char in '0123456789.':
                   y += char

        z = float(y)
        if z>30:
            z=0
            nurmber -= 1
        total += z

    print(x)

    if nurmber == 0: price= 'Fail'
    else: price = total/nurmber
    file.write(f'{x},{price},\n')


for x in data.dataset:
    getprice(x)
file.close()
