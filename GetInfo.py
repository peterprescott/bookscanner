#import necessary pieces
import datetime
import csv
import ScannerBrain

#give file unique date-time stamp so that it doesn't overwrite anything
now=str(datetime.datetime.now())
unique=''
for char in now:
	if char in '0123456789':
		unique += char

file = open(f'AutoFiles\{unique}Basement2BookData.csv','w')

#import sold ISBN codes
with open('basementSHELF.txt', newline='') as csvfile:
    list = []
    sold = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in sold:
           list += row

for x in list:
	print(x)

for x in list:
	ScannerBrain.info(x,file)
