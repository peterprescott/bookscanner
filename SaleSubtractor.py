#import necessary pieces
import datetime
import csv

#give file unique date-time stamp so that it doesn't overwrite anything
now=str(datetime.datetime.now())
unique=''
for char in now:
	if char in '0123456789':
		unique += char

file = open(f'{unique}UpdatedStock.csv','w')
otherfile = open(f'{unique}RecentSales.csv','w')

otherfile.write(f'These are the books we have sold since we last updated our stock. {now}\n\n')


#import sold ISBN codes
with open('sales.csv', newline='') as csvfile:
    list = []
    sold = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in sold:
           list += row

total = 0
count = 2

with open('Master.csv', newline='') as csvfile:
    master = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in master:
        x=row[3]
        if x not in list:
            file.write(f'{row[0]},{row[1]},{row[2]},{row[3]},{row[4]},{row[5]}\n')
        if x in list:
            otherfile.write(f'{row[0]},{row[1]},{row[2]},{row[3]},{row[4]},{row[5]}\n')
            total += float(row[4])
            count +=1
            list.remove(x)

file.close()
otherfile.write(f'\nTotal Sales of £{total},,,Check,=sum(e3:e{count})\n\nWe weren\'t sure what to do with, {list}.\n\nRemember to check {unique}UpdatedStock.csv and resave it as Master.csv!')
otherfile.close()
