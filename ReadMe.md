You need to have Python installed. This can be done from: https://www.python.org/downloads/

***SaleSubtractor***
This is a simple program to update the Master.csv stocklist, by removing the records of those sold books whose ISBN barcodes have been scanned as they have been sold.

1. Open Sales.txt and delete any data that is there from previously.

2. Input the ISBN barcodes of the books that have been sold since the last update. This is done by plugging in the Barcode Reader and scanning the Data Upload barcode (on the card in the box in which the Barcode Reader was bought).

3. This should result in a column of 13-digit ISBN codes.

4. Then save your updated Sales.txt file.

5. You can now run SaleSubtractor.py. Just double-click it and it will run. It should finish in less than a second and close automatically, creating two new .csv files: RecentSales.csv and UpdatedStock.csv (with both names prefaced by a unique date-time stamp from the moment the program was run to make sure that nothing is accidentally overwritten).

6. Now you can open the RecentSales.csv file to see a list of recently sold books (ie. a list of books whose barcodes were recognized from the Barcode Reader's uploaded data), which has also calculated the total value of recently sold books (this could be checked against receipts). Obviously barcodes not on the stocklist (perhaps second-hand books; or non-books with barcodes, eg. DVDs, CDs, cards) will not be included on this list. You should compare RecentSales.csv with Sales.csv to see what, if anything, has been missed. You could use https://www.justbooks.co.uk/ to investigate what the missed barcodes actually were (if they were books).

7. Now you can open the UpdatedStock.csv file. It should have six columns (Title, Author, Publisher, ISBN, Price, Case) and 1000+ rows of data (which should represent all the new books for sale in the shop). Compare it with the Master.csv file -- it should have x less rows, where x is the number of sales identified on RecentSales.csv. If not, there is a problem. But all being well, you can now delete the old Master.csv file and rename your new UpdatedStock.csv file as Master.csv.

8. Don't forget to also update the stocklist on the website! Log-in to www.treeoflife.org.uk/wp-admin/ and then go to TablePress (on the left-hand column) and 'Import a Table'. 'Choose File' and make sure that it will 'Replace existing table', selecting 'ID 2: master.csv'. Then click 'Import'. And check on http://www.treeoflife.org.uk/bookshop/ that this has worked successfully.


***BookScanner.***
To use BookScanner, you need to have installed the Python modules 'requests' and 'beautifulsoup4'.
See here http://docs.python-requests.org/en/master/user/install/ and here https://pypi.org/project/beautifulsoup4/

Double-click the file to run it.

The program will invite you to enter an ISBN, which can be done swiftly with a plugged in Barcode Reader.

The program will first attempt to download the book data from OpenLibrary.org's API (this is very quick); if this fails it will try and manually pull the data from JustBooks.co.uk (this takes a little longer).

You don't need to wait for the program to ask for another ISBN before scanning another book -- it will catch up in due course!

When you are finished press 'q' (lower-case) to quit instead of entering another ISBN.

The program will then put all the scanned ISBNs and found data into a date-timestamped file called BookData.csv. Occasionally a stray comma means that the data is not aligned with the proper columns (in due course this could be fixed). The columns are also in a different order from the Master.csv file (this too could be coordinated), so watch this as you add data from BookData.csv to Master.csv.
