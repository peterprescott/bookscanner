import requests
import json
import datetime
from random import randint
from bs4 import BeautifulSoup

def blank(x,file):
#instead of previous non-attempt [below], use requests and beautiful soup to get author, title, publisher, publication date from
	jbquestion = f'https://www.justbooks.co.uk/search/?author=&title=&lang=en&isbn={x}&new=1&used=1&ebooks=1&destination=gb&currency=GBP&mode=basic&st=sr&ac=qr'
	print('OpenLibrary.org does not have the data. Give us a minute while we look elsewhere...')
	jbanswer = requests.get(jbquestion, headers={'Accept': 'text/plain'})
	jbtxt = jbanswer.text
	soup = BeautifulSoup(jbtxt, 'html.parser')

	soupauthor = soup.find_all(attrs={'itemprop':"author"})
	souptitle = soup.find_all(attrs={'itemprop':"name"})
	souppublisher = soup.find_all(attrs={'itemprop':"publisher"})

	if bool(soupauthor)==True:
		print('That worked!')
		soupauthor = soupauthor[0].get_text()
		souptitle = souptitle[0].get_text()
		souppublisher= souppublisher[0].get_text()
	else:
		print('Alas, unsuccesful. Are you sure that number was an ISBN?')
		soupauthor = 'Not Found'
		souptitle = ''
		souppublisher = ''

	soupauthor=soupauthor.replace(',','')
	souptitle=souptitle.replace(',','')

	souprrp = ''
	now=str(datetime.datetime.now())
	writesoup = str(soup)
	file.write(f'{now},{x},{soupauthor},{souppublisher},{souptitle},,,,\n')


def info(isbn,file):

	res = 'https://openlibrary.org/api/books?bibkeys=ISBN:'+isbn+'&jscmd=data&format=json'
	book = requests.get(res, headers={'Accept': 'text/plain'})
	txt=book.text
	#remove tab spaces
	txt = txt.replace('\\t', '')
	txt = txt.replace('\\r', '')
	txt = txt.replace('\\n', '')

	if txt == '{}':
		blank(isbn,file)
		return
	dtxt=json.loads(txt)
	etxt = [v for (k,v) in dtxt.items()]
	tx=etxt[0]
#	wanted = ['authors', 'title', 'subtitle',  'publishers', 'publish_date', 'weight', 'number_of_pages', 'cover', ]
	wanted = ['subtitle', 'weight', 'number_of_pages', ]
	record = {k:v for k,v in tx.items() if k in wanted}

	now=str(datetime.datetime.now())

	twostep = ['title', 'authors', 'publishers', 'cover', 'publish_date']
	recordtwo = {k:v for k,v in tx.items() if k in twostep}

	#this is a bit of a fudge--if the data included multiple authors/publishers you will only get back one--and if there isn't any author/publisher data the program will crash.
	title=recordtwo.get('title').replace(',','')
	if bool(recordtwo.get('authors'))==True:
		author=recordtwo.get('authors').pop().get('name').replace(',','')
	else: author=''
	if bool(recordtwo.get('publishers'))==True:
		publisher=recordtwo.get('publishers').pop().get('name').replace(',','')
	else: publisher=''
	if bool(recordtwo.get('publish_date'))==True:
		publish_date=recordtwo.get('publish_date').replace(',','')
	else: publish_date=''

	txt = ''

	for x in wanted:
		txt += f'{record.get(x)},'
	file.write(f'{now},{isbn},{author},{publisher},{publish_date},{title},{txt}\n')
