import requests
import json
import datetime
import ScannerBrain
from random import randint
from bs4 import BeautifulSoup
import csv


now=str(datetime.datetime.now())
unique=''
for char in now:
	if char in '0123456789':
		unique += char
isbn=''
file = open(f'AutoFiles\{unique}BasementBookData.csv','w')
file.write('Time Scanned, ISBN, Author, Publisher, Publication Date, Title, Subtitle, Weight, Number of Pages,\n')

#import basement ISBN codes
with open('BookDataInput.py', newline='') as csvfile:
    numbers = []
    basement = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in basement:
           numbers += row

for x in numbers:
    print(x)
for x in numbers:
    ScannerBrain.info(x,file)

file.close()


# from Bookscanner.py
#       while isbn!='q':
# 		print('What\'s the next ISBN? (or q to quit if you are finished)')
# 		isbn = str(input())
# 		if isbn == 'q':
# 			file.close()
# 			print(f'The data should be in {unique}BookData.csv')
# 			quit()
# 		ScannerBrain.info(isbn,file)
